var Sequelize = require('sequelize');
    q = require('q');


var sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: 'smart_strength.db',
    logging: false
});

var Workout = sequelize.define('Workout', {
    nodeid: Sequelize.INTEGER,
    proto: Sequelize.INTEGER,
    force: Sequelize.FLOAT,
    work: Sequelize.FLOAT,
    power: Sequelize.FLOAT,
    noreps: Sequelize.INTEGER,
    timestamp: Sequelize.INTEGER
}, {
    tableName: 'workout',
    timestamps: false
});

var Power = sequelize.define('Power', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.FLOAT,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'power',
    timestamps: false
});

var Work = sequelize.define('Work', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.INTEGER,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'power',
    timestamps: false
});

var Dist = sequelize.define('Dist', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.INTEGER,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'dist',
    timestamps: false
});

var Time = sequelize.define('Time', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.FLOAT,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'time',
    timestamps: false
});

function getWorkout(workout_id) {
    // returns a promise
    function processSubquery(recs) {
        var array = []
        recs.forEach(function(recs) {
            array.push(recs.value);
        });
        return array;
    }

    return Workout.find(workout_id).then(function(workout_record) {
        if (workout_record) {
            var workout = {
                force: workout_record.force,
                powerarray: [],
                workarray: [],
                work: workout_record.work,
                distarray: [],
                i: workout_record.id,
                power: workout_record.power,
                noreps: workout_record.noreps,
                nodeid: workout_record.nodeid,
                proto: workout_record.proto,
                timestamp: workout_record.timestamp,
                timearray: []
            };
            var subquery_params = {
                where: {
                    'workout_id': workout_id
                },
                order: 'sequence'
            };
            return q.all([
                Power.findAll(subquery_params).then(processSubquery),
                Work.findAll(subquery_params).then(processSubquery),
                Dist.findAll(subquery_params).then(processSubquery),
                Time.findAll(subquery_params).then(processSubquery)
            ]).spread(function(powerarray, workarray, distarray, timearray) {
                workout.powerarray = powerarray;
                workout.workarray = workarray;
                workout.distarray = distarray;
                workout.timearray = timearray;
                return workout;
            })
        } else {
            return {};
        }
    });
}

getWorkout(9).then(function(workout) {
    console.log(workout);
});