var express = require('express'),
    app = express(),
    url = require('url'),
    Sequelize = require('sequelize');
    q = require('q');

var sequelize = new Sequelize('database', 'username', 'password', {
    dialect: 'sqlite',
    storage: 'ss.db',
    logging: false
});

var Workout = sequelize.define('Workout', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    nodeid: Sequelize.INTEGER,
    proto: Sequelize.INTEGER,
    force: Sequelize.FLOAT,
    work: Sequelize.FLOAT,
    power: Sequelize.FLOAT,
    noreps: Sequelize.INTEGER,
    timestamp: Sequelize.INTEGER
}, {
    tableName: 'workout',
    timestamps: false
});

var Power = sequelize.define('Power', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.FLOAT,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'power',
    timestamps: false
});

var Work = sequelize.define('Work', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.INTEGER,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'power',
    timestamps: false
});

var Dist = sequelize.define('Dist', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.INTEGER,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'dist',
    timestamps: false
});

var Time = sequelize.define('Time', {
    rowid: { type: Sequelize.INTEGER, primaryKey: true },
    workout_id: Sequelize.INTEGER,
    value: Sequelize.FLOAT,
    sequence: Sequelize.INTEGER
}, {
    tableName: 'time',
    timestamps: false
});

app.get(['/api/btn', '/*/api/btn'], function(req, res) {
    var url_parts = url.parse(req.url, true);
    console.log(url_parts.query);
    if (url_parts.query.button && url_parts.query.button.toLowerCase() === 'last') {
        maxWorkoutId().then(function(workout_id) {
            if (workout_id == NaN) {
                res.json({});
            } else {
                getWorkout(workout_id).then(function(workout) {
                    res.json(workout);
                });
            }
        })
    } else if (url_parts.query.i) {
        getWorkout(url_parts.query.i).then(function(workout) {
            if (workout) {
                res.json(workout);
            } else {
                res.json({});
            }
        });
    } else {
        res.json({});
    }
});

app.get(['/api/new', '/*/api/new'], function(req, res) {
    res.json({hi: 'there'});
});

app.get(['/api/all', '/*/api/all'], function(req, res) {
    getAllWorkouts().then(function(workouts) {
        res.json(workouts);
    });

});

app.use(express.static(__dirname + '/public'));

app.listen(process.env.PORT || 8080);

function maxWorkoutId() {
    return Workout.max('rowid');
}

function getAllWorkouts() {

    return Workout.findAll({order: 'rowid DESC'})
        .then(function(workout_records) {
            var promises = [];
            
            workout_records.forEach(function(workout_record) {
                promises.push(getWorkout(workout_record.rowid));
            });
            return q.all(promises).then(function(workouts) {
                return workouts;
            });
        });
}

function getWorkout(workout_id) {
    // returns a promise
    function processSubquery(recs) {
        var array = []
        recs.forEach(function(recs) {
            array.push(recs.value);
        });
        return array;
    }

    return Workout.find(workout_id).then(function(workout_record) {
        if (workout_record) {
            var workout = {
                force: workout_record.force,
                powerarray: [],
                workarray: [],
                work: workout_record.work,
                distarray: [],
                i: workout_record.rowid,
                power: workout_record.power,
                noreps: workout_record.noreps,
                nodeid: workout_record.nodeid,
                proto: workout_record.proto,
                timestamp: workout_record.timestamp,
                timearray: []
            };
            var subquery_params = {
                where: {
                    'workout_id': workout_id
                },
                order: 'sequence'
            };
            return q.all([
                Power.findAll(subquery_params).then(processSubquery),
                Work.findAll(subquery_params).then(processSubquery),
                Dist.findAll(subquery_params).then(processSubquery),
                Time.findAll(subquery_params).then(processSubquery)
            ]).spread(function(powerarray, workarray, distarray, timearray) {
                workout.powerarray = powerarray;
                workout.workarray = workarray;
                workout.distarray = distarray;
                workout.timearray = timearray;
                return workout;
            })
        } else {
            return {};
        }
    });
}