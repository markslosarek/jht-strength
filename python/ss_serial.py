__author__ = 'mark.kannel'

import serial
import time
import datetime
import json
import sys
import sqlite3

HEADER = 0xFFFFFFFF
FOOTER = 0xFF00FF00
HOLDDIVISOR = 48000.0 #Just a WAG for now
DISTSCALE = 0.687 #inches
POWERSCALE = 1356.0 
FORCESCALE  = 0.030230
ADCOFFSET = -4627.56
SQLDB = '/home/pi/application/ss.db';

ser = serial.Serial('/dev/ttyAMA0', 9600, timeout=2)
# get_settings_from_database()


if ser.isOpen():
    print "Port opened is " + ser.name

ser.flushInput()

# wait for Arduino to boot (it boots when serial is re-opened)
time.sleep(1)

def uart_generator():
    global HEADER
    global FOOTER

    header_or_footer = 1

    # return_code = None

    # one_byte = 0

    _blob = bytearray(0)
    started = False

    while True:
        one_byte = ser.read(1)
        # print ord(one_byte) if len(one_byte) else ''
        one_byte = ord(one_byte) if len(one_byte) else 0

        header_or_footer = ((header_or_footer << 8) | one_byte) & 0xFFFFFFFF  # just keep shifting bytes in

        if header_or_footer == HEADER:  # until the header number is seen (4 bytes )
            print "header is found"
            started = True
        elif header_or_footer == FOOTER:  # until the header number is seen (4 bytes )
            print "footer is found"
            started = False
            return _blob
            break
        elif started:
            _blob.append(one_byte)
            # print one_byte

def get_settings_from_database():

    con = sqlite3.connect(SQLDB)
    c = con.cursor()

    c.execute("SELECT * FROM setting")
    s =  c.fetchall()

    settings = {}
    for r in s:
        settings[r[0]] = r[1]

    if con:
        con.close()

    return settings

def unpack(set_data):
    settings = get_settings_from_database()

    _holddivisor = float(settings['HOLDDIVISOR']) if 'HOLDDIVISOR' in settings else HOLDDIVISOR
    _distscale = float(settings['DISTSCALE']) if 'DISTSCALE' in settings else DISTSCALE
    _powerscale = float(settings['POWERSCALE']) if 'POWERSCALE' in settings else POWERSCALE
    _forcescale  = float(settings['FORCESCALE']) if 'FORCESCALE' in settings else FORCESCALE
    _adcoffset = float(settings['ADCOFFSET']) if 'ADCOFFSET' in settings else ADCOFFSET

    # print set_data

    structable = {'timestamp': 0,  'nodeid': 0, 'userid': "", 'noreps': 0,  'force': 0, 'proto': 0, 'work': 0, 'power': 0, 
        'i': 0, 'timearray': [], 'distarray': [], 'workarray': [], 'powerarray': []}
    # print "4th element is " + str(set_data[3])
    # print "8th element is " + str(set_data[7])
    # print "14th element is " + str(set_data[13])

    structable['proto'] = set_data[0]
    structable['nodeid'] = set_data[2] << 8 | set_data[1]
    # user id is 6 bytes 3..8
    structable['noreps'] = set_data[9]
    _force = set_data[11] << 8 | set_data[10]  # some work to do here to make it signed

    # convert to signed the simple way
    if _force > 32767:
        _force = _force - 65536

    structable['force'] = round((1.0 * (_force + _adcoffset) * _forcescale), 2)


    # 0  high byte of time in milliseconds
    # 1  low byte of time in milliseconds
    # 2  Distance travelled in encoder counts

    structable['distarray'] = []
    structable['timearray'] = []
    structable['powerarray'] = []
    structable['workarray'] = []
    # print "this many reps " + str(structable['noreps'])
    for x in range(0, structable['noreps']):
        print " x = " + str(x)
        _time = (set_data[13 + x * 3] << 8) | set_data[14 + x * 3]
        _dist = set_data[15 + x * 3]
        _power = 0.001
        _work = 0.001

        if (_time != 0) and (_dist != 0):
            _work = structable['force'] * ((0.08333 * _dist) + (_time / _holddivisor)) #inch * lbf convert to ft-lbf
            _power = _powerscale * (1.0 * _work / _time)

        print "    time: " + str(_time)
        print "    dist: " + str(_dist)
        print "    power: " + str(round(_power, 2))
        print "    work: " + str(round(_work, 2))

        structable['distarray'].append(_dist)
        structable['timearray'].append(_time)
        structable['powerarray'].append(round(_power, 2))
        structable['workarray'].append(round(_work, 2))

        structable['work'] += round(_work, 2)
        structable['power'] += round(_power, 2)

    structable['timestamp'] = int(time.time())

    # print str(len(structable['distarray']))
    save_to_database(structable)


def save_to_database(structable):

    try:
        con = sqlite3.connect(SQLDB)
        c = con.cursor()

        print json.dumps(structable)

        workout = (
                structable['nodeid'],
                structable['proto'],
                structable['force'],
                structable['work'],
                structable['power'],
                structable['noreps'],
                structable['timestamp'],
            )

        c.execute("INSERT INTO workout ('nodeid', 'proto', 'force', 'work', 'power', 'noreps', 'timestamp') VALUES (?,?,?,?,?,?,?)", workout)
        c.execute("SELECT last_insert_rowid()")
        workoutid = c.fetchone()[0]

        print "Workout ID: " + str(workoutid)
        distarray = [(workoutid, val, idx) for idx, val in enumerate(structable['distarray'])]
        c.executemany("INSERT INTO dist ('workout_id', 'value', 'sequence') VALUES (?,?,?)", distarray)

        powerarray = [(workoutid, val, idx) for idx, val in enumerate(structable['powerarray'])]
        c.executemany("INSERT INTO power ('workout_id', 'value', 'sequence') VALUES (?,?,?)", powerarray)

        timearray = [(workoutid, val, idx) for idx, val in enumerate(structable['timearray'])]
        c.executemany("INSERT INTO time ('workout_id', 'value', 'sequence') VALUES (?,?,?)", timearray)

        workarray = [(workoutid, val, idx) for idx, val in enumerate(structable['workarray'])]
        c.executemany("INSERT INTO work ('workout_id', 'value', 'sequence') VALUES (?,?,?)", workarray)

        con.commit()
    except lite.Error, e:

        if con:
            con.rollback()

        print "Error %s:" % e.args[0]
        sys.exit(1)

    finally:
        if con:
            con.close()

while True:
    if ser.inWaiting() > 0:
        set_data = uart_generator()
        unpack(set_data)
