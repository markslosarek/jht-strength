from serial import Serial
import time

serialPort = Serial("/dev/ttyAMA0", 9600, timeout=2)
if (serialPort.isOpen() == False):
    serialPort.open()

outStr = '0' + '1' + '2'  # protocol and node
outStr = outStr + '3'+'3'+'7'+'5'+'4'+'6'  # user id
outStr = outStr + chr(3)
outStr = outStr + '0'+'3'  # force
outStr = outStr + 'Z'+'B'+'a'+'9'+'8'+'6'+'4'+'2'+'0'  # reps

serialPort.flushInput()
serialPort.flushOutput()
serialPort.flushOutput()
serialPort.flushInput()

time.sleep(2)
for i, a in enumerate(range(33, 34)):

    # Header
    serialPort.write(chr(255))
    serialPort.write(chr(255))
    serialPort.write(chr(255))
    serialPort.write(chr(255))

    serialPort.write(outStr)
	
	# Footer
    serialPort.write(chr(255))
    serialPort.write(chr(0))
    serialPort.write(chr(255))
    serialPort.write(chr(0))
    
    time.sleep(6)
    
    print outStr

serialPort.close()

